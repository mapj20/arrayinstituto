/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package instituto;

/**
 *
 * @author macbookpro
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Creo un arreglo de personas
        Personas[] lista=new Personas[2];
        
        //creo el objeto 1 de tipo persona
        Personas p1=new Personas();
        p1.setNombre("Luis");
        p1.setEdad(20);
        
        //Agrego el objeto 1 a el arreglo de personas
        lista[0]=p1;
        
        
        Personas p2=new Personas();
        p2.setNombre("Pedro");
        p2.setEdad(40);
        lista[1]=p2;
        
        // Crear un objeto de la clase Listaspersonas
        //para llamar al metodo promedio
        
        ListasPersonas lpersona=new ListasPersonas();
        //con ese objeto llamar el metodo promedioEdad
        // me retorna el resulatdo
        
        double resultado=lpersona.promedioEdad(lista);
        
        // imprimir el resultado
        System.out.println("el promedio es: "+resultado);
        
    }
    
}
